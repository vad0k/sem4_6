<?php
/**
 * Реализовать возможность входа с паролем и логином с использованием
 * сессии для изменения отправленных данных в предыдущей задаче,
 * пароль и логин генерируются автоматически при первоначальной отправке формы.
 */

// Отправляем браузеру правильную кодировку,
// файл index.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');
$user = 'u20391';
$pass = '8767552';
$db = new PDO('mysql:host=localhost;dbname=u20391', $user, $pass, array(PDO::ATTR_PERSISTENT => true));

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  // Массив для временного хранения сообщений пользователю.
  $messages = array();


  if (!empty($_COOKIE['save'])) {
    if (!empty($_COOKIE['password'])) {
      $messages[] = sprintf('Вы можете <a href="login.php">войти</a> с логином <strong>%s</strong>
        и паролем <strong>%s</strong> для изменения данных.',
        strip_tags($_COOKIE['login']),
        strip_tags($_COOKIE['password']));
    }
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('save', '', 23);
    setcookie('exist', '', 23);
    setcookie('login', '', 23);
    setcookie('password', '', 23);
 
  }



  // Складываем признак ошибок в массив.
  $errors = array();
  $errors['fio'] = !empty($_COOKIE['fio_error']);
  $errors['email'] = !empty($_COOKIE['email_error']);
  $errors['date'] = !empty($_COOKIE['date_error']);
  $errors['sex'] = !empty($_COOKIE['sex_error']);
  $errors['limbs'] = !empty($_COOKIE['limbs_error']);
  $errors['biography'] = !empty($_COOKIE['biography_error']);
  $errors['check'] = !empty($_COOKIE['check_error']);
  // Выдаем сообщения об ошибках.
  if (!empty($errors['fio'])) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('fio_error', '', 23);
    // Выводим сообщение.
    $messages[] = '<div class="">Заполните ФИО.</div>';
  }
  // TODO: тут выдать сообщения об ошибках в других полях.
  if (!empty($errors['email'])) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('email_error', '', 23);
    // Выводим сообщение.
    $messages[] = '<div class="">Заполните email.</div>';
}
if (!empty($errors['date'])) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('date_error', '', 23);
    // Выводим сообщение.
    $messages[] = '<div class="">Заполните дату рождения.</div>';
}
if (!empty($errors['sex'])) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('sex_error', '', 23);
    // Выводим сообщение.
    $messages[] = '<div class="">Заполните пол.</div>';
}
if (!empty($errors['limbs'])) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('limbs_error', '', 23);
    // Выводим сообщение.
    $messages[] = '<div class="">Заполните количество конечностей.</div>';
}
if (!empty($errors['biography'])) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('biography_error', '', 23);
    // Выводим сообщение.
    $messages[] = '<div class="">Заполните биографию.</div>';
}
if (!empty($errors['check'])) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('check_error', '', 23);
    // Выводим сообщение.
    $messages[] = '<div class="">Отметьте чекбокс.</div>';
}
if (!empty($_COOKIE['login_check'])) {
  $messages['save'] = '<div>Email занят</div>';
}
  // Складываем предыдущие значения полей в массив, если есть.
  // При этом санитизуем все данные для безопасного отображения в браузере.
  $values = array();
  $values['fio'] = empty($_COOKIE['fio_value']) ? '' : strip_tags($_COOKIE['fio_value']);
  // TODO: аналогично все поля.
  $values['email'] = empty($_COOKIE['email_value']) ? '' : strip_tags($_COOKIE['email_value']);
  $values['date'] = empty($_COOKIE['date_value']) ? '' : strip_tags($_COOKIE['date_value']);
  $values['sex'] = empty($_COOKIE['sex_value']) ? '' : strip_tags($_COOKIE['sex_value']);
  $values['limbs'] = empty($_COOKIE['limbs_value']) ? '' : strip_tags($_COOKIE['limbs_value']);
  $values['abilities'] = empty($_COOKIE['abilities_value']) ? '' : strip_tags($_COOKIE['abilities_value']);
  $values['biography'] = empty($_COOKIE['biography_value']) ? '' : strip_tags($_COOKIE['biography_value']);
  // Если нет предыдущих ошибок ввода, есть кука сессии, начали сессию и
  // ранее в сессию записан факт успешного логина.
  if (!empty($_COOKIE[session_name()]) &&
      session_start() && !empty($_SESSION['login'])) {
    // TODO: загрузить данные пользователя из БД          !!!!!!!!!!
    // и заполнить переменную $values,
    // предварительно санитизовав.

 
    // Подготовленный запрос. Не именованные метки.
    try {

        $stmt = $db->prepare("SELECT full_name FROM application WHERE email = ?");
        $stmt -> execute([$_SESSION['login']]);
        $values['fio'] = $stmt->fetchColumn();
        
        $values['email'] = $_SESSION['login'];
        
        $stmt = $db->prepare("SELECT date FROM application WHERE email = ?");
        $stmt->execute([$_SESSION['login']]);
        $values['date'] = $stmt->fetchColumn();
        
        $stmt = $db->prepare("SELECT sex FROM application WHERE email = ?");
        $stmt->execute([$_SESSION['login']]);
        $values['sex'] = $stmt->fetchColumn();
        
        $stmt = $db->prepare("SELECT limbs FROM application WHERE email = ?");
        $stmt->execute([$_SESSION['login']]);
        $values['limbs'] = $stmt->fetchColumn();
        
        $stmt = $db->prepare("SELECT abilities FROM application WHERE email = ?");
        $stmt->execute([$_SESSION['login']]);
        $values['abilities'] = $stmt->fetchColumn();
        
        $stmt = $db->prepare("SELECT biography FROM application WHERE email = ?");
        $stmt->execute([$_SESSION['login']]);
        $values['biography'] = $stmt->fetchColumn();
        

        
    }
    catch(PDOException $e){
        print('Error : ' . $e->getMessage());
        exit();
    } 

    $messages[] ='Вход с логином ';
    $messages[] = $_SESSION['login'];
  }

  // Включаем содержимое файла form.php.
  // В нем будут доступны переменные $messages, $errors и $values для вывода 
  // сообщений, полей с ранее заполненными данными и признаками ошибок.
  include('form.php');
}
// Иначе, если запрос был методом POST, т.е. нужно проверить данные и сохранить их в XML-файл.
else {
  // Проверяем ошибки.
  $errors = FALSE;
  if (!preg_match("#^[А-Яа-яЁё]+\s[А-Яа-яЁё]+\s[А-Яа-яЁё]+$#u", $_POST['fio'])) {
    // Выдаем куку на день с флажком об ошибке в поле fio.
    setcookie('fio_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    // Сохраняем ранее введенное в форму значение на месяц.
    setcookie('fio_value', $_POST['fio'], time() + 30 * 24 * 60 * 60);
  }

// *************
// TODO: тут необходимо проверить правильность заполнения всех остальных полей.
// Сохранить в Cookie признаки ошибок и значения полей.
// *************
if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
  // Выдаем куку на день с флажком об ошибке в поле fio.
  setcookie('email_error', '1', time() + 24 * 60 * 60);
  $errors = TRUE;
}
else {
  // Сохраняем ранее введенное в форму значение на месяц.
  setcookie('email_value', $_POST['email'], time() + 30 * 24 * 60 * 60);
}
if (empty($_POST['date'])) {
  // Выдаем куку на день с флажком об ошибке в поле fio.
  setcookie('date_error', '1', time() + 24 * 60 * 60);
  $errors = TRUE;
}
else {
  // Сохраняем ранее введенное в форму значение на месяц.
  setcookie('date_value', $_POST['date'], time() + 30 * 24 * 60 * 60);
}
if (empty($_POST['sex'])) {
  // Выдаем куку на день с флажком об ошибке в поле fio.
  setcookie('sex_error', '1', time() + 24 * 60 * 60);
  $errors = TRUE;
}
else {
  // Сохраняем ранее введенное в форму значение на месяц.
  setcookie('sex_value', $_POST['sex'], time() + 30 * 24 * 60 * 60);
}
if (empty($_POST['limbs'])) {
  // Выдаем куку на день с флажком об ошибке в поле fio.
  setcookie('limbs_error', '1', time() + 24 * 60 * 60);
  $errors = TRUE;
}
else {
  // Сохраняем ранее введенное в форму значение на месяц.
  setcookie('limbs_value', $_POST['limbs'], time() + 30 * 24 * 60 * 60);
}
if (empty($_POST['biography'])) {
  // Выдаем куку на день с флажком об ошибке в поле fio.
  setcookie('biography_error', '1', time() + 24 * 60 * 60);
  $errors = TRUE;
}
else {
  // Сохраняем ранее введенное в форму значение на месяц.
  setcookie('biography_value', $_POST['biography'], time() + 30 * 24 * 60 * 60);
}
setcookie('abilities_value', serialize($_POST['abilities']), time() + 30 * 24 * 60 * 60);
if (empty($_POST['check'])) {
  // Выдаем куку на день с флажком об ошибке в поле fio.
  setcookie('check_error', '1', time() + 24 * 60 * 60);
  $errors = TRUE;
}
else {
  // Сохраняем ранее введенное в форму значение на месяц.
  setcookie('check_value', $_POST['check'], time() + 30 * 24 * 60 * 60);
}




  if ($errors) {
    // При наличии ошибок перезагружаем страницу и завершаем работу скрипта.
    header('Location: index.php');
    exit();
  }
  else {
    // Удаляем Cookies с признаками ошибок.
    setcookie('fio_error', '', 23);
    // TODO: тут необходимо удалить остальные Cookies.
    setcookie('email_error', '', 23);
    setcookie('date_error', '', 23);
    setcookie('sex_error', '', 23);
    setcookie('limbs_error', '', 23);
    setcookie('biography_error', '', 23);
    setcookie('check_error', '', 23);
  }

  switch($_POST['sex']) {
    case 'm': {
        $sex='m';
        break;
    }
    case 'f':{
        $sex='f';
        break;
    }
};

switch($_POST['limbs']) {
    case '1': {
        $limbs='1';
        break;
    }
    case '2':{
        $limbs='2';
        break;
    }
    case '3':{
        $limbs='3';
        break;
    }
    case '4':{
        $limbs='4';
        break;
    }
};

$abilities = serialize($_POST['abilities']);

  // Проверяем меняются ли ранее сохраненные данные или отправляются новые.
  if (!empty($_COOKIE[session_name()]) &&
      session_start() && !empty($_SESSION['login'])) {
    // TODO: перезаписать данные в БД новыми данными,
    // кроме логина и пароля.
    $user = 'u20391';
    $pass = '8767552';
    $db = new PDO('mysql:host=localhost;dbname=u20391', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
    $messages[] = "email изменен не будет";
    // Подготовленный запрос. Не именованные метки.
    try {
        $stmt = $db->prepare("UPDATE application SET full_name = ?, date = ?, sex = ?, limbs = ?, abilities = ?, biography = ? WHERE email = ?");
        $stmt -> execute(array($_POST['fio'],$_POST['date'],$sex,$limbs,$abilities,$_POST['biography'],$_SESSION['login']));
    }
    catch(PDOException $e){
        print('Error : ' . $e->getMessage());
        exit();
    }
    // Сохраняем куку с признаком успешного сохранения.
    setcookie('save', '1');
  }
  else { // Только в случаи нового пользователя надо проверять логин
    //Проверяем логин на уникальность
      try {
      $stmt = $db->prepare("SELECT email FROM application WHERE email = ?");
      $stmt -> execute([$_POST['email']]);
      $log_check = $stmt->fetchColumn();
      }
      catch(PDOException $e){
          print('Error : ' . $e->getMessage());
          exit();
      }
      if ($log_check != NULL){
          setcookie('login_check', '1');
      }
      else {
            // Генерируем уникальный логин и пароль.
            // TODO: сделать механизм генерации, например функциями rand(), uniquid(), md5(), substr().
        $login = $_POST['email'];
        $pass = substr(uniqid(), 0, 8);
        // Сохраняем в Cookies.
        setcookie('login', $login);
        setcookie('pass', $pass);

    // TODO: Сохранение данных формы, логина и хеш md5() пароля в базу данных.
    $password_md5 = md5($password);
    try {
        $stmt = $db->prepare("INSERT INTO application SET full_name = ?, email = ?, date = ?, sex = ?, limbs = ?, abilities = ?, biography = ?, password = ?");
        $stmt -> execute(array($_POST['fio'],$_POST['email'],$_POST['date'],$sex,$limbs,$abilities,$_POST['biography'], $password_md5));
    }
    catch(PDOException $e){
        print('Error : ' . $e->getMessage());
        exit();
    }
    // Сохраняем куку с признаком успешного сохранения.
    setcookie('save', '1');
  }
  }
  header('Location: ./');
}
