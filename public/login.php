<link rel="stylesheet" href="login.css">
<link href="https://fonts.googleapis.com/css?family=Fira+Sans|Playfair+Display:400,700|Poppins:300&display=swap&subset=cyrillic-ext" rel="stylesheet">
<?php
    session_start();

    if(!empty($_SESSION['login'])){
        header('Location: ./form.php');
    }

    if($_SERVER['REQUEST_METHOD'] == 'GET'){  // Если пришли методом GET 
?>
<div class="form-wrapper">

    <div class="label  form-label ">Войти</div>
   <form class = 'form' action="" method="post">
        <?php if(!empty($_COOKIE['login_error'])){print('<div class="error-label label">Ошибка входа</div>'); setcookie('login_error',""); }  ?>
        <div class="login-input-wrapper input-wrapper">
            <div class="label  login-input-label ">E-mail:</div>
            <input type = "text" class = "input" name="email" />
        </div>
        <div class="pass-input-wrapper input-wrapper">
            <div class="label  pass-input-label">Password:</div>
            <input type = "password" class = "input" name="pass" />
        </div>
        <div class="sub-button-wrapper">
            <input class="sub-button" type="submit" value="Войти" />
        </div>
    </form>
    <div class="reg-button-wrapper">
        <a class = "reg-label label" href="index.php">Зарегистрироваться</a>
    </div>
</div>

<?php 
    } else { //Значит пришли методом POST
        if ($_POST['email'] == 'admin')
        {
            header('Location: ./admin.php');
            exit();
        } 
        $user = 'u20391';
        $pass = '8767552';
        $db = new PDO('mysql:host=localhost;dbname=u20391', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
        
        try {
            $stmt = $db->prepare("SELECT password FROM application WHERE email = ?");
            $stmt -> execute(array($_POST['email']));
            $pass1 = $stmt->fetchColumn();
            if (md5($_POST['pass']) == $pass1){
                // Если все ок, то авторизуем пользователя.
                $_SESSION['login'] = $_POST['email'];          
                // Делаем перенаправление.
                header('Location: ./form.php');
            }
            else {
                setcookie('login_error','true');
                header('Location: ./login.php');
            }
        }
        catch(PDOException $e){
            print('Error : ' . $e->getMessage());
            exit();
        } 

    }

?>