<?php 
// All function in same page
    function error_border($name){
        if ($GLOBALS['errors'][$name])
            print 'class = "error_border"';
    } 
    function error_underline($name){
        if ($GLOBALS['errors'][$name])
            print 'class = "error_underline"';
    }

    function values($name){
        print($GLOBALS['values'][$name]);
    } 

    function heigh_Form(){
      $height = 665;
        if (!empty($GLOBALS['message'])) {
            $height += count($GLOBALS['message'])*18;
        }
        print($height);
    }
   
?>
<link rel="stylesheet" href="style.css">
<form class = "form_wrapper" action="" method="post" height = '<?php heigh_Form()?>px'>
    <?php 
       if (!empty($messages)) {
        print('<div class = "err-mes-wrapper">');
        // Выводим все сообщения.
        foreach ($messages as $message) {
            print($message);
        }
        print('</div>');
        }
        ?>
    <div class="input-fio-wrapper">        
            <div class="input-fio_label">Write Fname and Lname</div>
            <input name="fio" <?php error_border('fio')?> value="<?php values('fio') ?>">
    </div>
    
    <div class="email-input-wrapper">
        <div class="email-input_label">Write e-mail</div>
        <input name="email" <?php error_border('email') ?> value="<?php values('email') ?>" >
    </div>

    <div class="year-input-wrapper">
        <div class="year-input_label">Write your birth day</div>
        <input name="date" type="date" <?php error_border('date') ?> value="<?php values('date') ?>">
    </div>

    <div class = "sex-input-wrapper" >
       <div class="sex-input_label">Choose your sex</div>
       <div class="sex-input">        
            <label <?php error_underline('sex') ?>>
                <input type="radio" value="m" name="sex" <?php if ($values['sex']=="male") {print 'checked';} ?>> Man
            </label>
            <label <?php error_underline('sex') ?>>
                <input type="radio" value="f" name="sex" <?php if ($values['sex']=="female") {print 'checked';} ?>>Female
            </label>
        </div>
    </div>

    <div class = "limbs-input-wrapper">
        <div class="limbs-input_label">Choose count of limbs</div>
        <label <?php error_underline('limbs')?>>
            <input type="radio" value="1" name="limbs" <?php if ($values['limbs']=="1") {print 'checked';} ?>>1
        </label>
        <label <?php error_underline('limbs')?>>
            <input type="radio" value="2" name="limbs" <?php if ($values['limbs']=="2") {print 'checked';} ?>>2
        </label>
        <label <?php error_underline('limbs')?>>
            <input type="radio" value="3" name="limbs" <?php if ($values['limbs']=="3") {print 'checked';} ?>>3
        </label>
        <label <?php error_underline('limbs') ?>>
            <input type="radio" value="4" name="limbs" <?php if ($values['limbs']=="4") {print 'checked';} ?>>4
        </label>
    </div>

    <div class = "abilities-input-wrapper">
            <div class="abilities-input_label">Choose abilities</div>
            <select name="abilities[]" multiple="multiple" >
                <option value="Immortality" <?php if (strripos($values['abilities'], "Immortality")) {print 'selected';} ?>>Immortality</option>
                <option value="Passing through walls" <?php if (strripos($values['abilities'], "Passing through walls")) {print 'selected';} ?>>Passing through walls</option>
                <option value="Levitation" <?php if (strripos($values['abilities'], "Levitation")) {print 'selected';} ?>>Levitation</option>
            </select><br>
    </div>
    <div class="biography-input-wrapper">
        <div class="biography-input_label">Write your Biography</div>
        <textarea name="biography" cols="30" rows="10" <?php error_border('biography')?>><?php values('biography') ?>
         </textarea>
    </div>
    <div class="check-input-wrapper">
        <div <?php error_underline('check') ?>>
            <input name="check" type="checkbox" <?php error_border('check') ?> >
            <div class="checl-input_label">I agree with the sending of data</div>
        </div>
    </div>
    <div class="submit-button">
        <input type="submit" value="Отправить">
    </div>
    <div class="login-href-wrapper">
        <div class="login-href">Если вы хотите выйти нажмите </div><a href="logout.php">сюда</a>
    </div>
      
    <div class="login-href-wrapper">
        <div class="login-href">Если вы уже зарегестрированы, перейдите </div><a href="login.php">сюда</a>
    </div>
</form>
